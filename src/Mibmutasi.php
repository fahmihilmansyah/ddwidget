<?php


namespace FhhLab\Nusantara;


use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\CurlHttpClient;

/**
 *
 */
class Mibmutasi
{
    public $client;
    public const BANK_URL = 'https://mib.bankmandiri.co.id/sme/common/login.do?action=logoutSME';
    public const URL_LOGIN = 'https://mib.bankmandiri.co.id/sme/common/login.do?action=loginSME';
    public const URL_MUTASI_FORM = 'https://mib.bankmandiri.co.id/sme/front/transactioninquiry.do?action=transactionByDateRequest&menuCode=MNU_GCME_040201';
    public const URL_VIEW_MUTASI = 'https://mib.bankmandiri.co.id/sme/front/transactioninquiry.do';
    public const URL_REQUESTMENU = 'https://mib.bankmandiri.co.id/sme/common/login.do?action=menuWithNameRequest';
    public const URL_REQUESTTOP = 'https://mib.bankmandiri.co.id/sme/common/login.do?action=topRequestSME';

    function __construct()
    {
        $this->client = new CurlHttpClient();
    }

    function testmib()
    {

//        $response = $this->client->request('GET', self::BANK_URL);
        $this->mibLogin();
        $this->mibrequesttop();
        exit;

    }

    function mibLogin()
    {
        $response = $this->client->request('POST', self::URL_LOGIN, [
            'body' => [
                'corpId' => 'AB35379',
                'userName' => 'DEWI01',
                'passwordEncryption' => '',
                'language' => 'en_US',
                'password' => 'Sattama1',
                'sessionId' => '',
                'ssoFlag' => ''
            ]
        ]);
        print_r($response->getContent());
    }
    function mibrequesttop(){
        $response = $this->client->request('GET', self::URL_REQUESTTOP);
        print_r($response->getContent());
    }
}