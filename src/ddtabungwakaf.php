<?php
/**
 * Created by PhpStorm.
 * Project : testproj
 * User: fahmihilmansyah
 * Date: 15/09/19
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 17.10
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */

namespace FhhLab\Nusantara;


use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\CurlHttpClient;

/**
 *
 */
class ddtabungwakaf
{
    public $client;
    public const BASE_URL = 'https://donasi.tabungwakaf.com/campaign/';
//    https://api.bawaberkah.org/api-v2/campaign/search?not_empty_donation=true&expired=false&sort=newest&limit=3&offset=0&type_id=0&category_id=0
//

    function __construct()
    {
        $this->client = new CurlHttpClient();
    }

    function getProdukdd(){


        $result =[];
        try {
            $res = $this->client->request('GET', self::BASE_URL,[]);
            $crawl = new Crawler($res->getContent());
            $res = ($res->getContent());
//            $data = $crawl->filterXPath('/html/body/div/div[3]/div/div/div/div[2]/div/div');
//            $data = $crawl->filterXPath('/html/body/div/div[3]/div/div/div/div[2]/div/div[1]');
            $data = $crawl->filter('.tidak_terbatas');
            $result['rc']=200;
            $result['message']='Success';
            $data->each(function (Crawler $node, $i) use (&$result) {
                $donasiterkumpulexp = explode('Rp',trim(($node->filter('.card-campaign-item-body>.card-meta>.row>.meta-total-donasi')->text())));
                $result['data'][]=array(
                    'img_link' =>($node->filter('.card-campaign-item-images>a>img')->extract(['src']))[0],
                    'url_link' =>($node->filter('.card-campaign-item-images>a')->extract(['href']))[0],
                    'title' =>($node->filter('.card-campaign-item-body>.campaign-title>a')->text()),
                    'description' =>($node->filter('.card-campaign-item-body>.card-meta>.meta-feature-text>p')->text()),
                    'donas_terkumpul'=>str_replace('.','', $donasiterkumpulexp[1])
                );
//                print_r(($node->filter('.card-campaign-item-body>.card-meta>.row>.meta-total-donasi')->text()));exit;
//                print_r($ass);exit;
//                print_r(trim(($node->filter('.card-campaign-item-body>.card-meta>.row>.meta-total-donasi')->text())));exit;
//                $result['data'][] = array(
//                    'img_link' => ($node->filter('.tidak_terbatas > .card-campaign-item-images > a > img')->extract(array('src')))[0],
//                    'text' => $node->filter('.card-campaign-item-body>.campaign-title > a')->text(),
//                    'url_link' => ($node->filter('.tidak_terbatas > .card-campaign-item-images > a')->extract(array('href')))[0],
//                );
            });
//            exit;
//            print_r($result);exit;
//            $res = json_decode($res,true);
//            $result['rc']=200;
//            $result['message']='Success';
//            $result['data']=$res['data'];
        }catch (\Exception $e){
            $result=array('rc'=>'404','message'=>'Error: '.$e->getMessage(),'data'=>[]);
            echo $e->getMessage()."\n";
        }
        return $result;
    }
}