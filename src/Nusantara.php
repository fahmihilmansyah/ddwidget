<?php


namespace FhhLab\Nusantara;


use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\CurlHttpClient;

/**
 *
 */
class Nusantara
{


    public const SCOPE_PROPINSI = 'prop';
    public const SCOPE_KABUPATEN = 'kab';
    public const SCOPE_KECAMATAN = 'kec';
    public const SCOPE_DESA = 'desa';

    public const NUSANTARA_URL = 'http://mfdonline.bps.go.id/index.php?link=hasil_pencarian';
    public const BANK_URL = 'https://mib.bankmandiri.co.id/sme/common/login.do?action=logoutSME';
    public const URL_LOGIN = 'https://mib.bankmandiri.co.id/sme/common/login.do?action=loginSME';
    public const URL_MUTASI_FORM = 'https://mib.bankmandiri.co.id/sme/front/transactioninquiry.do?action=transactionByDateRequest&menuCode=MNU_GCME_040201';
    public const URL_VIEW_MUTASI = 'https://mib.bankmandiri.co.id/sme/front/transactioninquiry.do';
    public const URL_REQUESTMENU = 'https://mib.bankmandiri.co.id/sme/common/login.do?action=menuWithNameRequest';
    public const URL_REQUESTTOP = 'https://mib.bankmandiri.co.id/sme/common/login.do?action=topRequestSME';

    private function request_data($scope = self::SCOPE_PROPINSI, $katakunci = '')
    {
        $client = new CurlHttpClient();

        return $client->request('POST', self::NUSANTARA_URL, [
            'body' => [
                'pilihcari' => $scope,
                'kata_kunci' => $katakunci,
            ]
        ]);
    }

    public function test($valdata = '')
    {
//        $seacrhValue = ['a','i','u','e','o'];
        $result = [];
//        foreach ($seacrhValue as $valdata):
            echo "Request Data ".$valdata. "\n";
            $response = $this->request_data(self::SCOPE_DESA, $valdata);
            $crawler = new Crawler($response->getContent());
            echo "Proses Data ".$valdata. "\n";
            $trs = $crawler->filterXPath('//tr[@class="table_content"]');
            $trs->each(function (Crawler $node, $i) use (&$result) {
                $tds = $node->filterXPath('//td');
//            echo  $node->text()."\n";
                $provinceCode = trim($tds->eq(1)->text());
                $provinceName = trim($tds->eq(2)->text());
                $kabCode = trim($tds->eq(3)->text());
                $kabName = trim($tds->eq(4)->text());
                $kecCode = trim($tds->eq(5)->text());
                $kecName = trim($tds->eq(6)->text());
                $desaCode = trim($tds->eq(7)->text());
                $desaName = trim($tds->eq(8)->text());
                $dataisi=array(
                    'provinceCode'=>$provinceCode,
                    'provinceName'=>$provinceName,
                    'kabCode'=>$kabCode,
                    'kabName'=>$kabName,
                    'kecCode'=>$kecCode,
                    'kecName'=>$kecName,
                    'desaCode'=>$desaCode,
                    'desaName'=>$desaName,
                );
                $pack = http_build_query($dataisi);
                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL,"localhost/linkaja/linkaja/web/site/importwilayah");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS,$pack);

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $server_output = curl_exec($ch);

                curl_close ($ch);

//            echo $provinceCode." ".$provinceName." ".$kabCode." ".$kabName." ".$kecCode." ".$kecName." ".$desaCode." ".$desaCode."\n";
//                $result[] = array(
//                    'ID' => (string)$provinceCode,
//                    'PARENT' => '',
//                    'NAME' => $provinceName,
//                    'KET' => 'PROV'
//                );
//                $result[] = array(
//                    'ID' => (string)$provinceCode . $kabCode,
//                    'PARENT' => (string)$provinceCode,
//                    'NAME' => $kabName,
//                    'KET' => 'KAB/KOTA'
//                );
//                $result[] = array(
//                    'ID' => (string)$provinceCode . $kabCode . $kecCode,
//                    'PARENT' => (string)$provinceCode . $kabCode,
//                    'NAME' => $kecName,
//                    'KET' => 'KEC'
//                );
//                $result[] = array(
//                    'ID' => (string)$provinceCode . $kabCode . $kecCode . $desaCode,
//                    'PARENT' => (string)$provinceCode . $kabCode . $kecCode,
//                    'NAME' => $desaName,
//                    'KET' => 'DESA'
//                );
            });
//        endforeach;
//        print_r($result);
//        $result = $this->unique_multi_array($result, ['ID', 'PARENT']);
//        echo json_encode($result);
//        print_r($response->getContent());
        exit;
        echo "adad";
    }


    function unique_multi_array($array, $key)
    {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach ($array as $val) {
            if (is_array($key)) {
                foreach ($key as $keys) {
                    if (!in_array($val[$keys], $key_array)) {
                        $key_array[$i] = $val[$keys];
                        $temp_array[$i] = $val;
                    }
                }
            } else {
                if (!in_array($val[$key], $key_array)) {
                    $key_array[$i] = $val[$key];
                    $temp_array[$i] = $val;
                }
            }

            $i++;
        }
        return $temp_array;
    }

    function testmib()
    {

        $client = new CurlHttpClient();
        $response = $client->request('GET', self::BANK_URL);
        print_r($response->getContent());
        exit;

    }
    function mibLogin($client){

    }
}