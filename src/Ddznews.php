<?php
/**
 * Created by PhpStorm.
 * Project : testproj
 * User: fahmihilmansyah
 * Date: 15/09/19
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 17.10
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */

namespace FhhLab\Nusantara;


use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\CurlHttpClient;

/**
 *
 */
class Ddznews
{
    public $client;
    public const BASE_URL = 'https://znews.id/category/news/beritapilihan/';

    function __construct()
    {
        $this->client = new CurlHttpClient();
    }

    function getProdukdd(){

        $result =[];
        try {
            $res = $this->client->request('GET', self::BASE_URL);
            $crawl = new Crawler($res->getContent());
//        $data = $crawl->filterXPath('//*[@id="main"]/div/ul');
//            $data = $crawl->filterXPath('//*[@id="main"]/div/ul/li');
//        $data = $crawl->filter('.item-details');
            $dataisi=null;
            for ($xs=0;$xs<=20;$xs++){
                $modisi = $crawl->filter('.td-ss-main-content > .td_module_'.$xs);
                if($modisi->count() > 0){
                    $dataisi = $modisi;
                }
            }
//        $data = $crawl->filter('.td_module_11');
        $data = $dataisi;// $crawl->filter('.td_module_11');
            $result['rc']=200;
            $result['message']='Success';
            $data->each(function (Crawler $node, $i) use (&$result) {
                if($i < 5) {
                    $img_url = ($node->filter('.td-module-thumb > a > img ')->extract(array('src')))[0];
                    $title = $node->filter('.item-details > h3 > a ')->text();
                    $mini_desc = $node->filter('.item-details > .td-excerpt ')->text();
                    $url_link = ($node->filter('.item-details > h3 > a ')->extract(array('href')))[0];
                    $newcon = new CurlHttpClient();
                    $news = $newcon->request('GET', $url_link);
                    $crawnews = new Crawler($news->getContent());
                    $crawnews->filter('.td-post-content div')->each(function (Crawler $crawler) {
                        foreach ($crawler as $node) {
                            $node->parentNode->removeChild($node);
                        }
                    });
                    $datanews = $crawnews->filter('.td-post-content');
                    $resid = $datanews->html();
                    $result['data'][] = array(
                        'img_link' => $img_url,
                        'title' => $title,
                        'url_link' => $url_link,
                        'mini_content'=>trim($mini_desc),
                        'full_content' => strip_tags( trim($resid),'<hr><strong><p><img><br>')
                    );
                }
            });
        }catch (\Exception $e){
            $result=array('rc'=>'404','message'=>'Error: '.$e->getMessage(),'data'=>[]);
            echo $e->getMessage()."\n";
        }
        return $result;
    }
}