<?php
/**
 * Created by PhpStorm.
 * Project : testproj
 * User: fahmihilmansyah
 * Date: 15/09/19
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 17.10
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */

namespace FhhLab\Nusantara;


use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\CurlHttpClient;

/**
 *
 */
class Ddshop
{
    public $client;
    public const BASE_URL = 'http://shop.dompetdhuafa.org/shop/shop';

    function __construct()
    {
        $this->client = new CurlHttpClient();
    }

    function getProdukdd(){

        $result =[];
        try {
            $res = $this->client->request('GET', self::BASE_URL);
            $crawl = new Crawler($res->getContent());
//        $data = $crawl->filterXPath('//*[@id="main"]/div/ul');
            $data = $crawl->filterXPath('//*[@id="main"]/div/ul/li');
//        $data = $crawl->filter('.products, .columns-3 > li');
            $result['rc']=200;
            $result['message']='Success';
            $data->each(function (Crawler $node, $i) use (&$result) {
                $result['data'][] = array(
                    'img_link' => ($node->filter('.astra-shop-thumbnail-wrap>a > img')->extract(array('src')))[0],
                    'text' => $node->filter('.astra-shop-summary-wrap > a')->text(),
                    'url_link' => ($node->filter('.astra-shop-summary-wrap > a')->extract(array('href')))[0],
                );
            });
        }catch (\Exception $e){
            $result=array('rc'=>'404','message'=>'Error: '.$e->getMessage(),'data'=>[]);
            echo $e->getMessage()."\n";
        }
        return $result;
    }
}