<?php
/**
 * Created by PhpStorm.
 * Project : testproj
 * User: fahmihilmansyah
 * Date: 15/09/19
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 17.10
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */

namespace FhhLab\Nusantara;


use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\CurlHttpClient;

/**
 *
 */
class Ddvolunteer
{
    public $client;
    public const BASE_URL = 'https://volunteer.dompetdhuafa.org/wp-admin/admin-ajax.php?action=list_aksi';
//    https://api.bawaberkah.org/api-v2/campaign/search?not_empty_donation=true&expired=false&sort=newest&limit=3&offset=0&type_id=0&category_id=0
//https://api.bawaberkah.org/api-v2/campaign/group?group_name=mariberbagi&limit=3&offset=0&category_id=0

    function __construct()
    {
        $this->client = new CurlHttpClient();
    }

    function getProdukdd(){


        $result =[];
        try {
            $res = $this->client->request('GET', self::BASE_URL);
            $res = ($res->getContent());
            $res = json_decode($res,true);
            $result['rc']=200;
            $result['message']='Success';
            $result['data']=$res;
        }catch (\Exception $e){
            $result=array('rc'=>'404','message'=>'Error: '.$e->getMessage(),'data'=>[]);
            echo $e->getMessage()."\n";
        }
        return $result;
    }
}